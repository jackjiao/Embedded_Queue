#include "Queue.h"

#define QUEUE_BUF1_LENGTH	100
#define QUEUE_BUF2_LENGTH	100

QueueType QueueBuf1[QUEUE_BUF1_LENGTH];
QueueType QueueBuf2[QUEUE_BUF2_LENGTH];

struct QueueBuffer MyQueueBuffer1,MyQueueBuffer2;

void Queue_Init(void)
{
	MyQueueBuffer1.pBufferStart=MyQueueBuffer1.pIn=MyQueueBuffer1.pOut=&QueueBuf1[0];
	MyQueueBuffer1.pBufferEnd=&QueueBuf1[QUEUE_BUF1_LENGTH-1];
	MyQueueBuffer1.mCount=0;

	MyQueueBuffer2.pBufferStart=MyQueueBuffer2.pIn=MyQueueBuffer2.pOut=&QueueBuf2[0];
	MyQueueBuffer2.pBufferEnd=&QueueBuf2[QUEUE_BUF2_LENGTH-1];
	MyQueueBuffer2.mCount=0;
	
}
void Queue_Destory(void)
{
	Queue_Clear(&MyQueueBuffer1);
	Queue_Clear(&MyQueueBuffer2);
}
void Queue_Push(struct QueueBuffer *pQueueBuffer,QueueType mData)
{
	QueueType *p=pQueueBuffer->pIn;
	*p=mData;

	if(p==pQueueBuffer->pBufferEnd)
	{
		p=pQueueBuffer->pBufferStart;
	}
	else
	{
		p++;
	}
	pQueueBuffer->pIn=p;

	pQueueBuffer->mCount++;
}
QueueType Queue_Pop( struct QueueBuffer  *pQueueBuffer )
{
	QueueType mData;
	QueueType *p=pQueueBuffer->pOut;

	mData=*p;

	if(p==pQueueBuffer->pBufferEnd)
	{
		p=pQueueBuffer->pBufferStart;
	}
	else
	{
		p++;
	}
	
	pQueueBuffer->pOut=p;

	if(pQueueBuffer->mCount>0)
	pQueueBuffer->mCount--;

	return mData;

}
unsigned int Queue_Num( struct QueueBuffer  *pQueueBuffer )
{
	return pQueueBuffer->mCount;
}
void Queue_Clear( struct QueueBuffer  *pQueueBuffer )
{
	//DisEnable Inturrupt
	pQueueBuffer->pIn=pQueueBuffer->pOut=pQueueBuffer->pBufferStart;
	pQueueBuffer->mCount=0;
	//Enable Interrupt
}
