#ifndef _QUEUE_H
#define _QUEUE_H

typedef unsigned int QueueType;
struct QueueBuffer {
	QueueType *pBufferStart;
	QueueType *pBufferEnd;
	QueueType *pIn;
	QueueType *pOut;
	unsigned int mCount;
};

void Queue_Init(void);
void Queue_Destory(void);
void Queue_Push(struct QueueBuffer *pQueueBuffer,QueueType mData);
QueueType Queue_Pop( struct QueueBuffer  *pQueueBuffer );
unsigned int Queue_Num( struct QueueBuffer  *pQueueBuffer );
void Queue_Clear( struct QueueBuffer  *pQueueBuffer );


extern struct QueueBuffer MyQueueBuffer1,MyQueueBuffer2;
#endif
