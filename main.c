#include "stdio.h"
#include "Queue.h"
#include "stdlib.h"

unsigned char tempbufer[10];
int main()
{
	unsigned int i;
	unsigned int num;
	Queue_Init();
	printf("Queue1 Init!\r\n");
	for(i =0;i<10;i++)
	{
		tempbufer[i]=i;
		Queue_Push(&MyQueueBuffer1,tempbufer[i]);
	}
	printf("Queue1 Push Success!\r\n");
	num=Queue_Num(&MyQueueBuffer1);
	printf("Queue1 Count is %d\r\n",num);
	for(i=0;i<num/2;i++)
	{
		printf("Pop%d: value:%d\r\n",i,Queue_Pop(&MyQueueBuffer1));
	}
	num=Queue_Num(&MyQueueBuffer1);
	printf("Queue1 Count is %d\r\n",num);

	for(i =0;i<10;i++)
	{
		tempbufer[i]=20+i;
		Queue_Push(&MyQueueBuffer1,tempbufer[i]);
	}
	printf("Queue1 Push Success!\r\n");
	num=Queue_Num(&MyQueueBuffer1);
	printf("Queue1 Count is %d\r\n",num);
	for(i=0;i<num;i++)
	{
		printf("Pop%d: value:%d\r\n",i,Queue_Pop(&MyQueueBuffer1));
	}
	num=Queue_Num(&MyQueueBuffer1);
	printf("Queue1 Count is %d\r\n",num);
	system("PAUSE");
}